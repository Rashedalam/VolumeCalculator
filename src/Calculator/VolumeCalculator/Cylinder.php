<?php

namespace Pondit\Calculator\VolumeCalculator;


class Cylinder
{
    public $pi;
    public $radius;
    public $height;

    public function get_area(){
        return $this->pi*$this->radius*$this->radius*$this->height;
    }
}