<?php
include_once "vendor/autoload.php";

use Pondit\Calculator\VolumeCalculator\Cube;

$cube=new Cube();
$cube->radius=10;
echo $cube->cube_area();