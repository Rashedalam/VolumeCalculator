<?php

include_once 'vendor/autoload.php';

use Pondit\Calculator\VolumeCalculator\Volume;

$volume=new Volume();
$volume->length=10;
$volume->width=10;
$volume->height=10;

echo $volume->get_area();