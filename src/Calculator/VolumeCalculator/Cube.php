<?php


namespace Pondit\Calculator\VolumeCalculator;


class Cube
{
    public $radius;

    public function cube_area()
    {
        return $this->radius * $this->radius * $this->radius;
    }
}