<?php

include_once 'vendor/autoload.php';

use Pondit\Calculator\VolumeCalculator\Cone;

$cone=new Cone();
$cone->pi=3.1416;
$cone->height=10;
$cone->radius=5;

var_dump($cone->get_area());