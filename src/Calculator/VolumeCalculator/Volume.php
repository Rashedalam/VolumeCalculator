<?php

namespace Pondit\Calculator\VolumeCalculator;


class Volume
{
    public $length;
    public $width;
    public $height;

    public function get_area()
    {
        return $this->height * $this->width * $this->length;
    }
}