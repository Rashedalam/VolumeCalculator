<?php

namespace Pondit\Calculator\VolumeCalculator;


class Cone
{
    public $radius;
    public $pi;
    public $height;

    public function get_area(){
        return $this->radius*$this->pi*$this->height;
    }
}