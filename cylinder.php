<?php

include_once 'vendor/autoload.php';

use Pondit\Calculator\VolumeCalculator\Cylinder;

$cylinder=new Cylinder();
$cylinder->height=10;
$cylinder->radius=5;
$cylinder->pi=3.1416;

var_dump($cylinder->get_area());